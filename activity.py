from collections import defaultdict

class Graph:
    def __init__(self, nodes):
        self.nodes = nodes
        self.graph = defaultdict(list)

    def add_edge(self, source, destination, weight=1):
        self.graph[source].append((destination, weight))

    def print_edges(self):
        for node in self.graph:
            for edge in self.graph[node]:
                print(f"{node} -> {edge[0]} (Weight: {edge[1]})")

    def bfs(self, start_node):
        visited = set()
        queue = [start_node]
        path = []

        while queue:
            node = queue.pop(0)
            path.append(node)

            if node not in visited:
                visited.add(node)
                for edge in self.graph[node]:
                    queue.append(edge[0])

        return path

    def bfs_target_node_search(self, start_node, target_node):
        visited = set()
        queue = [(start_node, [start_node])]

        while queue:
            node, path = queue.pop(0)
            visited.add(node)

            if node == target_node:
                return path

            for edge in self.graph[node]:
                if edge[0] not in visited:
                    queue.append((edge[0], path + [edge[0]]))

        return None

    def dfs(self, start_node):
        visited = set()
        path = []

        def dfs_recursive(node):
            visited.add(node)
            path.append(node)

            for edge in self.graph[node]:
                if edge[0] not in visited:
                    dfs_recursive(edge[0])

        dfs_recursive(start_node)
        return path


nodes = ['A', 'B', 'C', 'D', 'E', 'F', 'G']


graph = Graph(nodes)


graph.add_edge('A', 'B', 2)
graph.add_edge('A', 'C', 3)
graph.add_edge('B', 'D', 4)
graph.add_edge('C', 'D', 1)
graph.add_edge('C', 'E', 5)
graph.add_edge('D', 'E', 6)
graph.add_edge('E', 'F', 7)
graph.add_edge('F', 'G', 8)


print("Graph edges:")
graph.print_edges()


print("BFS Traversal:")
bfs_path = graph.bfs('A')
print(bfs_path)


print("BFS Target Node Search:")
target_node = 'F'
bfs_target_path = graph.bfs_target_node_search('A', target_node)
print(bfs_target_path)


nodes2 = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H']


graph2 = Graph(nodes2)


graph2.add_edge('A', 'B', 2)
graph2.add_edge('A', 'C', 3)
graph2.add_edge('B', 'D', 4)
graph2.add_edge('C', 'E', 5)
graph2.add_edge('D', 'E', 6)
graph2.add_edge('F', 'G', 8)
graph2.add_edge('G', 'H', 9)


print("BFS Target Node Search in disconnected graph:")
